# frozen_string_literal: true

module Container
  class Failure < StandardError; end

  # Container runner base class
  #
  # This class is used as a base class for all container runners
  # that run arbitrary rake tasks which require specific
  # ecosystem containers
  #
  class Base < ApplicationService
    # Initialize container runner
    #
    # @param [String] package_ecosystem
    # @param [String] task_name
    # @param [Array] task_args
    def initialize(package_ecosystem:, task_name:, task_args: [])
      @package_ecosystem = package_ecosystem
      @task_name = task_name
      @task_args = task_args
    end

    def call
      log(:info, "Starting dependency updater")
    end

    private

    attr_reader :package_ecosystem, :task_name, :task_args

    # Unique name postfix for updater
    #
    # @return [String]
    def unique_name_postfix
      @unique_name_postfix ||= "#{SecureRandom.alphanumeric(9)}-#{SecureRandom.alphanumeric(5)}".downcase
    end

    # Rake task string
    #
    # @return [String]
    def rake_task
      @rake_task ||= "dependabot:#{task_name}[#{task_args.join(',')}]"
    end
  end
end
