# frozen_string_literal: true

module V2
  class API < Grape::API
    prefix "api"

    version "v2", using: :path

    mount Hooks::Project
    mount Hooks::System

    mount NotifyRelease
    mount Projects
    mount Users
    mount MergeRequests
    mount UpdateJobs

    add_swagger_documentation(
      models: [
        Project::Entity,
        Configuration::Entity,
        Registries::Entity,
        User::Entity
      ]
    )
  end
end
