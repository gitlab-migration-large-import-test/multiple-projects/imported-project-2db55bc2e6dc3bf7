import { defineConfig, devices } from "@playwright/test";

/**
 * See https://playwright.dev/docs/test-configuration.
 */
export default defineConfig({
  testDir: "./spec/e2e/tests",
  /* Run tests in files in parallel */
  fullyParallel: false,
  /* Fail the build on CI if you accidentally left test.only in the source code. */
  forbidOnly: !!process.env.CI,
  /* Do not retry tests */
  retries: 0,
  /* Reporter to use. See https://playwright.dev/docs/test-reporters */
  reporter: [["list"], ["junit", { outputFile: "tmp/playwright.xml" }]],
  /* Shared settings for all the projects below. See https://playwright.dev/docs/api/class-testoptions. */
  use: {
    /* Base URL to use in actions like `await page.goto('/')`. */
    baseURL: `http://${process.env.APP_HOST || "localhost"}:3000`,
    ...devices["Desktop Chrome"]
  },
  /* Configure projects for major browsers */
  projects: [
    {
      name: "setup",
      testMatch: "global-setup\.ts"
    },
    {
      name: "api",
      testMatch: "api/**/*.spec.ts",
      dependencies: ["setup"]
    },
    {
      name: "ui",
      testMatch: "ui/**/*.spec.ts",
      dependencies: ["setup"]
    }
  ]
});
