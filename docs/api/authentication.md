# Authentication

The API uses [Basic Authentication](https://en.wikipedia.org/wiki/Basic_access_authentication) to authenticate requests. Authentication is enabled when application setting [SETTINGS__ANONYMOUS_ACCESS](../config/environment.md#application) is set to `false` and will require a username and password that matche a user account which can be created via:

- rake task `rake user:create` (see [rake tasks](../administration/rake.md#create-user))
- api endpoint `POST /api/v2/users` (see [add user](../api/reference.md#add-user))

example:

```sh
curl -H "Content-Type: application/json" -H "Authorization: Basic $(echo -n 'username:password' | base64)" https://example.com/api/v2/projects
```
