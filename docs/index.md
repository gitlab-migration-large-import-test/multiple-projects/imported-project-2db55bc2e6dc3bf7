---
layout: home

hero:
  name: dependabot-gitlab
  text: orchestrator for dependabot-core library to create dependency update merge requests for GitLab projects
  actions:
    - theme: brand
      text: Get Started
      link: /guide/index.md
    - theme: alt
      text: View on GitLab
      link: https://gitlab.com/dependabot-gitlab/dependabot

features:
  - title: Cloud native install or simple standalone execution
    details: Deploy on kubernetes and manage all projects from a central persisted location or run as a stateless script from your GitLab CI setup
  - title: Security updates
    details: Automatically update dependencies with detected security vulnerabilities
  - title: Integration with GitLab via webhooks
    details: Integrate with GitLab to automatically merge dependency update merge requests, closed outdated update merge requests, automatically add projects as soon as dependabot.yml was committed in to repository and more
---
